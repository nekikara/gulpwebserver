var gulp      = require('gulp'),
    webserver = require('gulp-webserver');

gulp.task('proxy', function() {
    gulp.src('./proxy')
        .pipe(webserver({
            host:     '0.0.0.0',     // 指定しなければ、localhostになる。この設定出ないと外部からアクセスできない。
            port:     '9000',        // 指定しなければ、8000番ポート
            proxies:  [
                {source: '/hoge', target: 'http://0.0.0.0:9010/'}, // 0.0.0.0:9000/hogeでアクセスすれば、下の
                                                                   // appにアクセスする
                {source: '/fuga', target: 'http://0.0.0.0:9011/'}  // 0.0.0.0:9000/fugaでは、publicにアクセス
            ],
        }));
});

gulp.task('app', function() {
    gulp.src('./app')
        .pipe(webserver({
            host:   '0.0.0.0',
            port:   '9010',
        }));
});

gulp.task('public', function() {
    gulp.src('./public')
        .pipe(webserver({
            host:   '0.0.0.0',
            port:   '9011',
        }));
});

gulp.task('default', ['proxy', 'app', 'public']);     // gulpコマンドでのデフォルトで動作するタスクを登録
